A-Calc Console (Test)
==========
A-Calc Console (Test)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar create-project jakharbek/a-calc-console-php-test:dev-master
```  

 
DIRECTORY STRUCTURE
-------------------

```
application             contains application
    input/              contains input files
    output/             contains output results
docker/                 contains docker
```

Usage
-----
First you need to copy the .env.example file and rename it to .env

Then fill out everything as you need to work after you can run docker-compose

```php
docker-compose up -d
```
If you run this command for the first time, then it will probably download not-available images of the docker and this may take some time.

After you should set write permissions, to do this, run this command.

```php
docker-compose exec app chmod 777 -R /app/application
```

Now you need to install all the dependencies of the compositor application, to do this, run this command.

```php
docker-compose exec app composer install --working-dir=/app/application
```
Everything is ready now you can use the console command of our application

Command Calc
------

<b>[filename]</b> - The file will be used from the folder input

<b>[operation]</b> - Operation list: 

command:
```php
docker-compose exec app php /app/application/calc [filename] "[operation]"
```

After all calculations, the results will be saved in a folder <b>/output/</b>

Example:
```php
docker-compose exec app php /app/application/calc test-1.txt "+"
```
console example output:
```console
/app/application/output/17-07-2019-14-23-38-result_all.txt
/app/application/output/17-07-2019-14-23-38-result_negative.txt
/app/application/output/17-07-2019-14-23-38-result_positive.txt
 Successful!!! 
 ```


Format Input File
-----
The input file format is two numbers separated by a space.

For example:

```php
1 4
6 45
-74 22
-5 12
``` 
